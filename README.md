# Object Oriented Design Patterns Assignment 1
### Due date: 27th February 2017

## Assignment
The local tourist board have decided to create a computer application that will supply the tourist with information about various towns around the country. 

The application should provide information about hotels, restaurants, places of interest and things to do. Within each of these sections the name, description and location should be displayed.
In addition to this an overall description about the town and an image displaying a map of how to get to the town should be provided.

The application should be extensible, i.e. if a requirement is to add another town, the code should be written in such a manner that minimum modifications are required.

## Requirements
1. Using a design pattern, write an application that will implement the above requirements.
2. Include at least 4 towns in your application.
3. Write a report that describes how this application works and in particular how the design pattern used was applied to this problem.
Illustrate the use of the design pattern with the aid of UML diagram(s).
Describe how the user could extend the application with a 5th or 6th town and different requirements such as sports facilities.