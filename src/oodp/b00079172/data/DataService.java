package oodp.b00079172.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DataService {

	public String[] getDirectoryJson(String dirPath) {
		File townsDir = new File(dirPath);
		File[] townJsonFiles = townsDir.listFiles();
		String[] jsonArr = new String[townJsonFiles.length];

		for (int i = 0; i < townJsonFiles.length; i++) {
			jsonArr[i] = getJsonFromFile(townJsonFiles[i]);
		}
		return jsonArr;
	}

	public String getJsonFromFile(File jsonFile) {
		String json = "";
		FileReader reader = null;
		try {
			reader = new FileReader(jsonFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (reader != null) {
			try {
				int currChar = reader.read();
				while (currChar != -1) {
					json += (char) currChar;
					currChar = reader.read();
				}

				return json;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

}
