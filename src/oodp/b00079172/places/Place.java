package oodp.b00079172.places;

public class Place {

	protected String name;
	protected String description;
	protected String location;

	public Place(String placeName, String placeDescription, String placeLocation) {
		this.name = placeName;
		this.description = placeDescription;
		this.location = placeLocation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFormattedInfo() {
		String infoString = String.format("Name: \t%s\nDescription: \t%s\nLocation: \t%s\n",
				this.name, this.description, this.location);
		return infoString;
	}

}
