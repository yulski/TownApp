package oodp.b00079172.places;

public class PlaceOfInterest extends Place {

	private Town town;

	public PlaceOfInterest(String name, String description, String location) {
		super(name, description, location);
	}

	public PlaceOfInterest(String name, String description, String location, Town placeTown) {
		super(name, description, location);
		this.town = placeTown;
	}

	public Town getTown() {
		return town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

}
