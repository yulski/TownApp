package oodp.b00079172.places;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import oodp.b00079172.data.DataService;

public class TownFactory {

	private String townDirectory;
	private String imageDirectory;

	public TownFactory(String townDir, String imageDir) {
		this.townDirectory = townDir;
		this.imageDirectory = imageDir;
	}

	public Town[] makeTowns() {
		DataService dataService = new DataService();
		String[] jsonArr = dataService.getDirectoryJson(this.townDirectory);
		Town[] towns = new Town[jsonArr.length];
		for (int i = 0; i < jsonArr.length; i++) {
			towns[i] = this.makeTownFromJson(jsonArr[i]);
		}
		return towns;
	}

	public Town makeTownFromJson(String json) {
		JsonObject jsonObj = new Gson().fromJson(json, JsonObject.class);

		String townName = jsonObj.get("name").getAsString();
		String townDescription = jsonObj.get("description").getAsString();
		String townLocation = jsonObj.get("location").getAsString();
		String imagePath = jsonObj.get("image").getAsString();
		Image image = null;
		try {
			image = ImageIO.read(new File(this.imageDirectory + imagePath));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Town town = new Town(townName, townDescription, townLocation, image);
		JsonArray jsonPlaces = jsonObj.get("placesOfInterest").getAsJsonArray();

		PlaceOfInterest[] placesOfInterest = new PlaceOfInterest[jsonPlaces.size()];

		for (int i = 0; i < jsonPlaces.size(); i++) {
			JsonObject currObj = jsonPlaces.get(i).getAsJsonObject();
			String placeName = currObj.get("name").getAsString();
			String placeDescription = currObj.get("description").getAsString();
			String placeLocation = currObj.get("location").getAsString();
			placesOfInterest[i] = new PlaceOfInterest(placeName, placeDescription, placeLocation,
					town);
		}

		town.setPlacesOfInterest(placesOfInterest);

		return town;
	}

}
