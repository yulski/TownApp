package oodp.b00079172.places;

import java.awt.Image;

public class Town extends Place {

	private Image image;
	private PlaceOfInterest[] placesOfInterest;

	public Town(String name, String description, String location, Image townImage) {
		super(name, description, location);
		this.image = townImage;
	}

	public Town(String name, String description, String location, Image townImage,
			PlaceOfInterest[] townPlacesOfInterest) {
		super(name, description, location);
		this.image = townImage;
		this.placesOfInterest = townPlacesOfInterest;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public PlaceOfInterest[] getPlacesOfInterest() {
		return placesOfInterest;
	}

	public void setPlacesOfInterest(PlaceOfInterest[] placesOfInterest) {
		this.placesOfInterest = placesOfInterest;
	}

}
