package oodp.b00079172.test;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import oodp.b00079172.data.DataService;

public class DataServiceTest {

	@Test
	public void testGetTownsJson() {
		DataService service = new DataService();
		String expected = "{a: {b: [c, d]}}";

		String actual = service.getJsonFromFile(new File("./test-data/json/test.json"));
		JsonParser parser = new JsonParser();
		JsonElement expectedElement = parser.parse(expected);
		JsonElement actualElement = parser.parse(actual);

		assertEquals(expectedElement, actualElement);
	}

}
