package oodp.b00079172.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;

import oodp.b00079172.data.DataService;
import oodp.b00079172.places.PlaceOfInterest;
import oodp.b00079172.places.Town;
import oodp.b00079172.places.TownFactory;

public class TownFactoryTest {

	@Test
	public void testMakeTownFromJson() {
		TownFactory factory = new TownFactory("./test-data/towns/", "./test-data/images/");

		PlaceOfInterest poi = new PlaceOfInterest("The Spire",
				"A big spire in the middle of O'Connel Street", "The middle of O'Connel Street");
		Town expected = null;
		try {
			expected = new Town("Dublin", "Capital of Ireland", "Leinster",
					ImageIO.read(new File("./test-data/images/test_image.jpg")),
					new PlaceOfInterest[] { poi });
			poi.setTown(expected);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String json = new DataService().getJsonFromFile(new File("./test-data/towns/town1.json"));

		Town actual = factory.makeTownFromJson(json);
		PlaceOfInterest[] pois = actual.getPlacesOfInterest();

		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(expected.getLocation(), actual.getLocation());
		// assertEquals(expected.getImage(), actual.getImage()); -- can't really
		// test image equality
		assertEquals(expected.getPlacesOfInterest().length, pois.length);
		assertEquals(poi.getName(), pois[0].getName());
		assertEquals(poi.getDescription(), pois[0].getDescription());
		assertEquals(poi.getLocation(), pois[0].getLocation());
		assertEquals(poi.getTown().getName(), pois[0].getTown().getName());
	}

}
