package oodp.b00079172.gui;

import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionListener;

import oodp.b00079172.places.PlaceOfInterest;
import oodp.b00079172.places.Town;

@SuppressWarnings("serial")
public class TownPanel extends PlacePanel implements ListViewPanel {

	private static final int IMAGE_WIDTH = 350;
	private static final int IMAGE_HEIGHT = 250;
	private static final String VIEW_TEXT = "View";
	private static final String PLACES_TEXT = "Places of Interest";

	private JLabel imageLabel;
	private JLabel placesLabel;
	private JList<String> list;
	private JButton view;

	public TownPanel(Town town) {
		super(town);
	}

	protected void configure() {
		Town town = (Town) this.place;

		Image scaledImage = town.getImage().getScaledInstance(TownPanel.IMAGE_WIDTH,
				TownPanel.IMAGE_HEIGHT, Image.SCALE_SMOOTH);
		this.imageLabel = new JLabel(new ImageIcon(scaledImage));
		this.addToCenter(this.imageLabel, 0, 1);

		PlaceOfInterest[] placesOfInterest = town.getPlacesOfInterest();
		String[] placeNames = new String[placesOfInterest.length];
		for (int i = 0; i < placesOfInterest.length; i++) {
			placeNames[i] = placesOfInterest[i].getName();
		}

		this.placesLabel = new JLabel(TownPanel.PLACES_TEXT, SwingConstants.CENTER);

		this.addToCenter(this.placesLabel, 1, 0);

		this.list = new JList<>();
		this.list.setListData(placeNames);
		this.addToCenter(this.list, 2, 0);

		this.view = new JButton(TownPanel.VIEW_TEXT);
		this.addToCenter(this.view, 3, 0);
	}

	public void addListSelectionListener(ListSelectionListener listener) {
		this.list.addListSelectionListener(listener);
	}

	public void addViewActionListener(ActionListener listener) {
		this.view.addActionListener(listener);
	}

	public String getSelectedValue() {
		return this.list.getSelectedValue();
	}

}
