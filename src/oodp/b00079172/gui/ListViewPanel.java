package oodp.b00079172.gui;

import java.awt.event.ActionListener;

import javax.swing.event.ListSelectionListener;

public interface ListViewPanel {

	public String getSelectedValue();

	public void addViewActionListener(ActionListener listener);

	public void addListSelectionListener(ListSelectionListener listener);

}
