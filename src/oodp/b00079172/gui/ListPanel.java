package oodp.b00079172.gui;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionListener;

@SuppressWarnings("serial")
public class ListPanel extends JPanel implements ListViewPanel {

	private static final String NAME = "ListPanel";
	private static final String VIEW_TEXT = "View";
	private static final int LIST_WIDTH = 250;
	private static final int LIST_HEIGHT = 250;

	JList<String> list;
	JButton view;

	public ListPanel(String[] listData) {
		this.setName(ListPanel.NAME);

		this.list = new JList<>();
		list.setPreferredSize(new Dimension(ListPanel.LIST_WIDTH, ListPanel.LIST_HEIGHT));
		list.setListData(listData);

		this.view = new JButton(ListPanel.VIEW_TEXT);

		this.add(this.list);
		this.add(this.view);
	}

	public String getSelectedValue() {
		return this.list.getSelectedValue();
	}

	public void addListSelectionListener(ListSelectionListener listener) {
		this.list.addListSelectionListener(listener);
	}

	public void addViewActionListener(ActionListener listener) {
		this.view.addActionListener(listener);
	}

}
