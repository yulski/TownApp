package oodp.b00079172.gui;

import java.util.Stack;

@SuppressWarnings("serial")
public class TownContainer extends SwitchablePanel {

	private Stack<String> history;

	public TownContainer(TownPanel townPanel) {
		super(townPanel);
		this.history = new Stack<>();
		this.setName(townPanel.getName());
	}

	public TownContainer(TownPanel townPanel, PlaceOfInterestPanel placePanel) {
		super(townPanel, placePanel);
		this.history = new Stack<>();
		this.setName(townPanel.getName());
	}

	public void showPrimary() {
		super.showPrimary();
		this.history.push(this.primaryName);
	}

	public void showSecondary() {
		super.showSecondary();
		this.history.push(this.secondaryName);
	}

}
