package oodp.b00079172.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import oodp.b00079172.places.PlaceOfInterest;
import oodp.b00079172.places.Town;

public class GUIFactory {

	public MainGUI makeGUI(Town[] towns) {
		MainGUI gui;
		PanelContainer container;
		ListPanel list;

		List<JPanel> townContainers = new ArrayList<>();
		String[] townNames = new String[towns.length];

		PlacePanelFactory panelFactory = new PlacePanelFactory();

		for (int i = 0; i < towns.length; i++) {
			townNames[i] = towns[i].getName();
			TownPanel townPanel = (TownPanel) panelFactory.makePlacePanel(towns[i]);
			TownContainer townContainer = new TownContainer(townPanel);
			PlaceOfInterest[] currPlaces = towns[i].getPlacesOfInterest();
			List<JPanel> placeOfInterestPanels = new ArrayList<>();
			for (int j = 0; j < currPlaces.length; j++) {
				placeOfInterestPanels.add(panelFactory.makePlacePanel(currPlaces[j]));
			}
			townContainer.setOthers(placeOfInterestPanels);
			townContainers.add(townContainer);
		}

		list = new ListPanel(townNames);

		container = new PanelContainer(list);
		container.setOthers(townContainers);

		gui = new MainGUI(container);

		return gui;
	}

}
