package oodp.b00079172.gui;

import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class SwitchablePanel extends JPanel {

	private CardLayout layout;
	protected JPanel primary;
	protected JPanel secondary;
	protected List<JPanel> others;
	protected String primaryName;
	protected String secondaryName;

	protected SwitchablePanel() {
	}

	public SwitchablePanel(JPanel primaryPanel) {
		this.primary = primaryPanel;
		this.secondary = null;
		this.primaryName = this.primary.getName();
		this.others = new ArrayList<>();

		this.layout = new CardLayout();
		this.setLayout(this.layout);
		this.layout.addLayoutComponent(this.primary, this.primaryName);
		this.add(this.primary);
	}

	public SwitchablePanel(JPanel primaryPanel, JPanel secondaryPanel) {
		this.primary = primaryPanel;
		this.secondary = secondaryPanel;
		this.primaryName = this.primary.getName();
		this.secondaryName = this.secondary.getName();
		this.others = new ArrayList<>();

		this.layout = new CardLayout();
		this.setLayout(this.layout);
		this.layout.addLayoutComponent(this.primary, this.primaryName);
		this.layout.addLayoutComponent(this.secondary, this.secondaryName);

		this.add(this.primary);
		this.add(this.secondary);
	}

	public void showPrimary() {
		this.layout.show(this, this.primaryName);
	}

	public void showSecondary() {
		this.layout.show(this, this.secondaryName);
	}

	public JPanel getPrimaryPanel() {
		return this.primary;
	}

	public void setPrimaryPanel(JPanel primaryPanel) {
		if (this.secondary != null) {
			this.layout.removeLayoutComponent(this.primary);
			this.remove(this.primary);
		}
		this.primary = primaryPanel;
		this.add(this.primary);
		this.primaryName = this.primary.getName();
		this.layout.addLayoutComponent(this.primary, this.primaryName);
	}

	public JPanel getSecondaryPanel() {
		return this.secondary;
	}

	public void setSecondaryPanel(JPanel secondaryPanel) {
		if (this.secondary != null) {
			this.layout.removeLayoutComponent(this.secondary);
			this.remove(this.secondary);
		}
		this.secondary = secondaryPanel;
		this.add(this.secondary);
		this.showPrimary();
		this.secondaryName = this.secondary.getName();
		this.layout.addLayoutComponent(this.secondary, this.secondaryName);
	}

	public void setOthers(List<JPanel> otherPanels) {
		this.others = otherPanels;
	}

	public List<JPanel> getOthers() {
		return this.others;
	}

}
