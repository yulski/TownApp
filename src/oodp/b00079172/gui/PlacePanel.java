package oodp.b00079172.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import oodp.b00079172.places.Place;

@SuppressWarnings("serial")
public abstract class PlacePanel extends JPanel {

	private static final Font TITLE_FONT = new Font("sansserif", Font.BOLD, 24);
	private static final String BACK_TEXT = "Back";
	private static final int INFO_PANE_WIDTH = 350;
	private static final int INFO_PANE_HEIGHT = 250;

	private JLabel title;
	private JTextPane infoPane;
	protected Place place;
	private JButton back;
	private JPanel center;
	private GridBagLayout centerLayout;
	private GridBagConstraints centerConstraints;

	public PlacePanel(Place place) {
		this.place = place;
		this.setName(this.place.getName());

		this.title = new JLabel(this.place.getName(), SwingConstants.CENTER);
		this.title.setFont(PlacePanel.TITLE_FONT);

		this.infoPane = new JTextPane();
		this.infoPane.setPreferredSize(
				new Dimension(PlacePanel.INFO_PANE_WIDTH, PlacePanel.INFO_PANE_HEIGHT));
		this.infoPane.setEditable(false);
		this.infoPane.setText(this.place.getFormattedInfo());

		this.back = new JButton(PlacePanel.BACK_TEXT);

		this.center = new JPanel();
		this.centerLayout = new GridBagLayout();
		this.centerConstraints = new GridBagConstraints();
		this.center.setLayout(this.centerLayout);
		this.centerConstraints.insets = new Insets(2, 8, 2, 8);

		this.centerConstraints.gridx = 0;
		this.centerConstraints.gridy = 0;
		this.center.add(this.infoPane, this.centerConstraints);

		this.setLayout(new BorderLayout());

		this.add(this.title, BorderLayout.NORTH);
		this.add(this.back, BorderLayout.SOUTH);
		this.add(this.center, BorderLayout.CENTER);

		this.configure();
	}

	protected abstract void configure();

	public void addBackActionListener(ActionListener listener) {
		this.back.addActionListener(listener);
	}

	protected void addToCenter(Component comp, int row, int col) {
		this.centerConstraints.gridx = col;
		this.centerConstraints.gridy = row;
		this.center.add(comp, this.centerConstraints);
	}

}
