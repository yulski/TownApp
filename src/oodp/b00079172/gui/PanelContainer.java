package oodp.b00079172.gui;

import java.util.Stack;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelContainer extends SwitchablePanel {

	private Stack<String> history;

	public PanelContainer(ListPanel listPanel) {
		super(listPanel);
		this.history = new Stack<>();
	}

	public PanelContainer(ListPanel listPanel, JPanel panel) {
		super(listPanel, panel);
		this.history = new Stack<>();
	}

	public void showPrimary() {
		super.showPrimary();
		this.history.push(this.primaryName);
	}

	public void showSecondary() {
		super.showSecondary();
		this.history.push(this.secondaryName);
	}

}
