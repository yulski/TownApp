package oodp.b00079172.gui;

import oodp.b00079172.places.Place;
import oodp.b00079172.places.PlaceOfInterest;
import oodp.b00079172.places.Town;

public class PlacePanelFactory {

	public PlacePanel makePlacePanel(Place place) {
		if (place instanceof Town) {
			Town town = (Town) place;
			TownPanel panel = new TownPanel(town);
			panel.setName(town.getName());
			return panel;
		} else {
			PlaceOfInterest poi = (PlaceOfInterest) place;
			PlaceOfInterestPanel panel = new PlaceOfInterestPanel(poi);
			panel.setName(poi.getName());
			return panel;
		}
	}

}
