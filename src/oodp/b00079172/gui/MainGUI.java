package oodp.b00079172.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class MainGUI extends JFrame {

	private static final String WINDOW_TITLE = "Town App";
	private static final String BANNER_TITLE = "Welcome to Town App!";
	private static final int WINDOW_WIDTH = 800;
	private static final int WINDOW_HEIGHT = 600;

	private JPanel banner;
	private PanelContainer container;
	// private PlacePanel[] places;
	private JScrollPane contentScrollPane;

	public MainGUI(PanelContainer panelContainer) {
		super(MainGUI.WINDOW_TITLE);
		this.container = panelContainer;
		// this.places = placePanels;

		this.banner = new JPanel();
		this.banner.setLayout(new FlowLayout(FlowLayout.CENTER));
		this.banner.add(new JLabel(MainGUI.BANNER_TITLE));

		this.contentScrollPane = new JScrollPane(this.container);

		this.add(this.banner, BorderLayout.NORTH);
		this.add(this.contentScrollPane, BorderLayout.CENTER);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(MainGUI.WINDOW_WIDTH, MainGUI.WINDOW_HEIGHT);
		this.setLocationRelativeTo(null);
	}

	public PanelContainer getPanelContainer() {
		return this.container;
	}

}
