package oodp.b00079172.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import oodp.b00079172.gui.ListPanel;
import oodp.b00079172.gui.MainGUI;
import oodp.b00079172.gui.PanelContainer;
import oodp.b00079172.gui.PlaceOfInterestPanel;
import oodp.b00079172.gui.TownContainer;
import oodp.b00079172.gui.TownPanel;
import oodp.b00079172.places.Town;

public class TownApp extends Thread {

	private static TownApp instance = null;

	private MainGUI gui;
	private Town[] towns;

	// private constructor to stop instantiation
	private TownApp(MainGUI applicationGUI, Town[] applicationTowns) {
		this.gui = applicationGUI;
		this.towns = applicationTowns;
	}

	private TownApp() {
	}

	public static TownApp getInstance() throws TownAppException {
		// create instance if it doesn't exist, otherwise throw exception
		if (TownApp.instance == null) {
			TownApp.instance = new TownApp();
			return TownApp.instance;
		} else {
			throw new TownAppException("An instance of TownApp already exists.");
		}
	}

	public void configure() {
		PanelContainer container = this.gui.getPanelContainer();
		ListPanel list = (ListPanel) container.getPrimaryPanel();

		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					String townName = list.getSelectedValue();
					for (JPanel panel : container.getOthers()) {
						TownContainer currTownContainer = (TownContainer) panel;
						String panelName = currTownContainer.getPrimaryPanel().getName();
						if (townName.equals(panelName)) {
							container.setSecondaryPanel(currTownContainer);
							container.showPrimary();
							break;
						}
					}
				}
			}
		});

		list.addViewActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				container.showSecondary();
			}
		});

		List<JPanel> townContainers = container.getOthers();
		for (JPanel currPanel : townContainers) {
			TownContainer townContainer = (TownContainer) currPanel;

			TownPanel townPanel = (TownPanel) townContainer.getPrimaryPanel();
			townPanel.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					if (!e.getValueIsAdjusting()) {
						String placeName = townPanel.getSelectedValue();
						for (JPanel otherPanel : townContainer.getOthers()) {
							PlaceOfInterestPanel poiPanel = (PlaceOfInterestPanel) otherPanel;
							String poiName = poiPanel.getName();
							if (placeName.equals(poiName)) {
								townContainer.setSecondaryPanel(poiPanel);
								townContainer.showPrimary();
								break;
							}
						}
					}
				}
			});
			townPanel.addViewActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					townContainer.showSecondary();
				}
			});
			townPanel.addBackActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					container.showPrimary();
				}
			});

			List<JPanel> others = townContainer.getOthers();
			for (JPanel otherPanel : others) {
				PlaceOfInterestPanel poiPanel = (PlaceOfInterestPanel) otherPanel;
				poiPanel.addBackActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						townContainer.showPrimary();
					}
				});
			}
		}
	}

	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				TownApp.this.gui.setVisible(true);
			}
		});
	}

	public MainGUI getMainGUI() {
		return this.gui;
	}

	public void setMainGUI(MainGUI applicationGUI) {
		this.gui = applicationGUI;
	}

	public Town[] getTowns() {
		return this.towns;
	}

	public void setTowns(Town[] applicationTowns) {
		this.towns = applicationTowns;
	}

}
