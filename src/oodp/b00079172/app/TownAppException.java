package oodp.b00079172.app;

@SuppressWarnings("serial")
public class TownAppException extends Exception {

	public TownAppException(String message) {
		super(message);
	}

}
