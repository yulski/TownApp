package oodp.b00079172.app;

public class TownAppDriver {

	public static void main(String[] args) {
		// use factory to create app
		TownApp app = TownAppFactory.makeApp();
		// if returned successfully, start the app
		if (app != null) {
			app.start();
		}
	}

}
