package oodp.b00079172.app;

import oodp.b00079172.gui.GUIFactory;
import oodp.b00079172.gui.MainGUI;
import oodp.b00079172.places.Town;
import oodp.b00079172.places.TownFactory;

public class TownAppFactory {

	private static final String TOWNS_PATH = "./data/towns/";
	private static final String IMAGES_PATH = "./data/images/";

	public static TownApp makeApp() {
		TownApp app = null;
		try {
			app = TownApp.getInstance();
		} catch (TownAppException e) {
			e.printStackTrace();
		}

		if (app != null) {
			TownFactory townFactory = new TownFactory(TownAppFactory.TOWNS_PATH,
					TownAppFactory.IMAGES_PATH);
			Town[] towns = townFactory.makeTowns();

			GUIFactory guiFactory = new GUIFactory();
			MainGUI gui = guiFactory.makeGUI(towns);

			app.setTowns(towns);
			app.setMainGUI(gui);

			app.configure();
		}

		return app;
	}

}
